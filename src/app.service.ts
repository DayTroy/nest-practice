import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return this.createHello();
  }
  createHello(): string {
    return 'Hello World!';
  }
  updateHello(): string {
    const helloArr = this.getHello().split(' ');
    helloArr.splice(1, 0, 'dear');
    return helloArr.join(' ');
  }
  deleteHello(): string {
    return '';
  }
}
