import { Controller, Get, Post, Put, Delete } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Post()
  createHello(): string {
    return this.appService.createHello();
  }
  @Put()
  updateHello(): string {
    return this.appService.updateHello();
  }
  @Delete()
  deleteHello(): string {
    return this.appService.deleteHello();
  }
}
